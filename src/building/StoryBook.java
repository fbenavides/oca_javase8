package building;

import library.Book;

public class StoryBook extends Book {
	
	public StoryBook()
	{
		//can access protected
		author = "Story test";
		modifyTemplate();
		
		Book book = new Book();
		
		//can't access protected
		//book.author = "not accessible";
		
		//cant access
		//book.issueCount = 5;
	}

}
