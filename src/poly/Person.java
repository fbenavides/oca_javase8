package poly;

public class Person {
	
	private String name;
	
	public Person(String nombre)
	{
		name = nombre;
	}
	
	public String getName()
	{
		return name;
	}
	
	public String toString()
	{
		return "Person class: " + getName();
	}

}


