package oca_javase8;

public class HelloExam {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(args[0]);
		System.out.println(args[1]);
		System.out.println(args[2]);
	}
	
	//STATIV PUBLIC
	/*static public void main(String[] args) {
		// TODO Auto-generated method stub
	}*/
	
	//VARARGS
	/*public static void main(String... arguments) {
	}*/
	
	//Overloading
	public static void main(String args) {
		System.out.println("Hello exam 2");
	}
	//Overloading
	public static void main(int number) {
		System.out.println("Hello exam 3");
	}

}
