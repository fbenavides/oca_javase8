package library;

public class Librarian {
	
	public Librarian()
	{
		Book book = new Book();
		
		book.author = "Librarian test";
		book.modifyTemplate();
		
		book.issueCount = 9;
	}

}
