package poly;

public class Student extends Person {
	
	private int SID;
	
	public Student(String nombre, int id)
	{
		super(nombre);
		this.SID = id;
	}
	
	public int getSID()
	{
		return SID;
	}

	@Override
	public String toString()
	{
		return "Student class: " + getSID() + ", name: " + super.toString();
	}
}
