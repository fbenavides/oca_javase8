package oca_javase8;

//compilation error, the public class/interface must be the name of the file
//public interface Printable {
//.. we are not detailing this part
//}

interface Movable {
//.. we are not detailing this part
}
