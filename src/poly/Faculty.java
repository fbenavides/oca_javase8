package poly;

public class Faculty extends Person {
	
	public Faculty(String nombre)
	{
		super(nombre);
	}

	@Override
	public String toString()
	{
		return "Faculty class " + super.toString() ;
	}
}
